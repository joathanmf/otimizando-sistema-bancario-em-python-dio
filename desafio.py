menu = """

[d] Depositar
[s] Sacar
[e] Extrato
[c] Cadastrar cliente
[b] Criar conta bancária
[q] Sair

=> """

limite = 500
LIMITE_SAQUES = 3

clientes = []
contas = []

clientes.append({
    "nome": "João",
    "data_nascimento": "01/01/1990",
    "cpf": "123",
    "endereco": "Rua A, 123 - Bairro C - Cidade/UF"
})

contas.append({
    "agencia": "0001",
    "numero": "1",
    "saldo": 0,
    "numero_saques": 0,
    "extrato": "",
    "cliente": clientes[0]
})

def cpf_valido(cpf):
    global clientes

    if not cpf.isdigit():
        print("Operação falhou! O CPF informado é inválido.")
        return False
    
    for cliente in clientes:
        if cliente["cpf"] == cpf:
            print("Operação falhou! Cliente já cadastrado.")
            return False
        
    return True


def operacao():
    cpf = input("Informe o CPF do cliente: ")
    cliente = buscar_cliente_por_cpf(clientes, cpf)

    if not cliente:
        print("Operação falhou! Cliente não encontrado.")
        return True, None

    print(f"Cliente: {cliente['nome']}")

    conta_bancaria = buscar_conta_por_cpf_cliente(contas, cpf)

    if not conta_bancaria:
        print("Operação falhou! Conta bancária não encontrada.")
        return True, None
    
    return False, conta_bancaria


def buscar_cliente_por_cpf(clientes, cpf):
    for cliente in clientes:
        if cliente["cpf"] == cpf:
            return cliente

    return None


def buscar_conta_por_cpf_cliente(contas, cpf):
    for conta in contas:
        if conta["cliente"]["cpf"] == cpf:
            return conta

    return None


def cadastrar_cliente():
    while True:
        cpf = input("Informe o CPF (Somente números): ")

        if not cpf_valido(cpf):
            continue


        nome = input("Informe o nome: ")
        data_nascimento = input("Informe a data de nascimento (dia/mês/ano): ")
        logradouro = input("Informe o logradouro: ")
        numero = input("Informe o número: ")
        bairro = input("Informe o bairro: ")
        cidade = input("Informe a cidade: ")
        estado = input("Informe o estado (UF): ")

        break

    endereco = f"{logradouro}, {numero} - {bairro} - {cidade}/{estado}"

    print(f"\nCliente {nome} cadastrado com sucesso!")

    return {"nome": nome, "data_nascimento": data_nascimento, "cpf": cpf, "endereco": endereco}


def criar_conta_bancaria(clientes, numero_conta):
    cpf = input("Informe o CPF do cliente: ")
    cliente = buscar_cliente_por_cpf(clientes, cpf)

    if cliente:
        conta = {
            "agencia": "0001",
            "numero": numero_conta,
            "saldo": 0,
            "numero_saques": 0,
            "extrato": "",
            "cliente": cliente,
        }

        print(f"Cliente: {cliente['nome']}")
        print("Conta bancária criada com sucesso!")
        print(f"\nAgência: {conta['agencia']} | Conta: {conta['numero']}")

        return conta
    
    return None


def depositar(saldo, valor, extrato, /): 
    if valor > 0:
        saldo += valor
        extrato += f"Depósito: R$ {valor:.2f}\n"

    else:
        print("Operação falhou! O valor informado é inválido.")

    print(f"\nDepósito de R$ {valor:.2f} realizado com sucesso!")
    
    return saldo, extrato


def sacar(*, saldo, valor, extrato, limite, numero_saques, LIMITE_SAQUES):
    excedeu_saldo = valor > saldo
    excedeu_limite = valor > limite
    excedeu_saques = numero_saques >= LIMITE_SAQUES

    if excedeu_saldo:
        print("Operação falhou! Você não tem saldo suficiente.")

    elif excedeu_limite:
        print("Operação falhou! O valor do saque excede o limite.")

    elif excedeu_saques:
        print("Operação falhou! Número máximo de saques excedido.")

    elif valor > 0:
        saldo -= valor
        extrato += f"Saque: R$ {valor:.2f}\n"
        numero_saques += 1

    else:
        print("Operação falhou! O valor informado é inválido.")

    print(f"\nSaque de R$ {valor:.2f} realizado com sucesso!")

    return saldo, extrato, numero_saques


def exibir_extrato(saldo, /, *, extrato):
    print()
    print("================ EXTRATO ================")
    print(extrato if extrato else "Não foram realizadas movimentações.")
    print(f"\nSaldo: R$ {saldo:.2f}")
    print("=========================================")


while True:
    opcao = input(menu)

    if opcao == "d":
        erro, conta_bancaria = operacao()

        if erro:
            continue

        saldo = conta_bancaria["saldo"]
        extrato = conta_bancaria["extrato"]

        valor = float(input("Informe o valor do depósito: "))

        conta_bancaria["saldo"], conta_bancaria["extrato"] = depositar(saldo, valor, extrato)

    elif opcao == "s":
        erro, conta_bancaria = operacao()

        if erro:
            continue

        saldo = conta_bancaria["saldo"]
        extrato = conta_bancaria["extrato"]
        numero_saques = conta_bancaria["numero_saques"]

        valor = float(input("Informe o valor do saque: "))

        conta_bancaria["saldo"], conta_bancaria["extrato"], conta_bancaria["numero_saques"] = sacar(
            saldo=saldo, valor=valor, extrato=extrato, limite=limite,
            numero_saques=numero_saques, LIMITE_SAQUES=LIMITE_SAQUES
        )

    elif opcao == "e":
        erro, conta_bancaria = operacao()

        if erro:
            continue

        saldo = conta_bancaria["saldo"]
        extrato = conta_bancaria["extrato"]

        print(f"Saldo: R$ {saldo:.2f}\n")
        print(extrato)

        exibir_extrato(saldo, extrato=extrato)

    elif opcao == "c":
        cliente = cadastrar_cliente()
        clientes.append(cliente)

    elif opcao == "b":
        numero_conta = len(contas) + 1
        conta_bancaria = criar_conta_bancaria(clientes, numero_conta)

        if conta_bancaria:
            contas.append(conta_bancaria)
        else:
            print("Operação falhou! Cliente não encontrado.")

    elif opcao == "q":
        break

    else:
        print("Operação inválida, por favor selecione novamente a operação desejada.")